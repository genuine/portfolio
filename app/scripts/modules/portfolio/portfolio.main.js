'use strict';

var $ = require('jquery');
var List = require('list.js');

module.exports = class Portfolio {
  constructor($el) {
    this.$el = $el;
    this.method(this.$el);

    var options = {
      valueNames: ['title', 'description', 'date', 'category'],
      listClass: 'media--portfolio-list'
    };

    var portfolioList = new List('portfolio', options);

  }
  method($element) {
    // console.log($element);
  }
};
