'use strict';

module.exports = ($el) => {

  require.ensure([], (require) => {

    var Module = require('./portfolio.main');
    new Module($el);

  });

};
